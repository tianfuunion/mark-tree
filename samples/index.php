<?php
declare (strict_types=1);

if (is_file(__DIR__ . '/../autoload.php')) {
    require_once __DIR__ . '/../autoload.php';
}
if (is_file(__DIR__ . '/../vendor/autoload.php')) {
    require_once __DIR__ . '/../vendor/autoload.php';
}

foreach (\mark\system\Os::getInfo() as $key => $item) {
    echo '<br>' . $key . ':' . var_export($item);
}
echo '<br>';
echo '<br>';