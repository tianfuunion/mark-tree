<?php
declare (strict_types=1);

namespace mark\tree;

/**
 * Class Tree
 *
 * @package mark\tree
 */
class Tree {
    public function __construct() {
    }

    public function get(): string {
        return 'this is a Mark Engine Tree Test.';
    }

    public function set(): void {
    }

    public function item(): void {
    }

    public function children(): void {
    }

    public function dump(): void {

    }

}